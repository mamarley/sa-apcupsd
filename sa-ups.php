#!/usr/bin/php
<?php
/*	sa-ups.php - A utility to expose the solar-assistant system as a UPS.
	Copyright © 2023 Michael Marley <michael@michaelmarley.com>
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once(dirname(__FILE__)."/Pses.php");
require_once(dirname(__FILE__)."/PsesMqtt.php");
require_once(dirname(__FILE__)."/config.php");

const VERSION="0.1";

class SaUpsStatus{
	public $dataCurrent=false;
	public $lastUpdateTime=null;
	
	public $gridAvailable=null;
	
	public $batteryLow=null;
	public $batteryPresent=null;
}

class SaUpsData{
	public $upsStatus;
	
	public $batteryCurrent=null;
	public $batteryFloatChargeVoltage=null;
	public $batteryShutdownCapacity=null;
	public $batteryStateOfCharge=null;
	public $batteryVoltage=null;
	
	public $gridPower=null;
	public $gridFrequency=null;
	public $gridVoltage=null;
	public $gridVoltageMax=null;
	public $gridVoltageMin=null;
	
	public $loadPower=null;
	public $loadFrequency=null;
	public $loadVoltage=null;
	
	public $batteryTemperature=null;
	public $inverterTemperature=null;
	public $loadPercentage=null;
	
	public $transferFrequencyHigh=null;
	public $transferFrequencyLow=null;
	public $transferVoltageHigh=null;
	public $transferVoltageLow=null;
	
	public $transferReason=null;
	
	function __construct(){
		$this->upsStatus=new SaUpsStatus();
	}
}

class SaUps{
	private const MQTT_TOPICS=[
		"solar_assistant/total/battery_state_of_charge/#",
		"solar_assistant/total/battery_temperature/#",
		"solar_assistant/total/grid_frequency/#",
		"solar_assistant/total/grid_power/#",
		"solar_assistant/total/grid_voltage/#",
		"solar_assistant/total/load_percentage/#",
		"solar_assistant/total/load_power/#",
		"solar_assistant/+/ac_output_frequency/#",
		"solar_assistant/+/ac_output_voltage/#",
		"solar_assistant/+/battery_current/#",
		"solar_assistant/+/battery_float_charge_voltage/#",
		"solar_assistant/+/battery_voltage/#",
		"solar_assistant/+/grid_frequency_high/#",
		"solar_assistant/+/grid_frequency_low/#",
		"solar_assistant/+/grid_voltage_high/#",
		"solar_assistant/+/grid_voltage_low/#",
		"solar_assistant/+/output_shutdown_capacity/#",
		"solar_assistant/+/temperature/#"
	];
	
	private $pses;
	private $psesMqtt;
	private $protocolDrivers=array();
	
	private $upsData;

	private $batteryCurrent=array();
	private $batteryVoltage=array();
	private $batteryFloatChargeVoltage=array();
	private $batteryShutdownCapacity=array();

	private $loadFrequency=array();
	private $loadVoltage=array();
	
	private $inverterTemperature=array();
	
	private $transferFrequencyHigh=array();
	private $transferFrequencyLow=array();
	private $transferVoltageHigh=array();
	private $transferVoltageLow=array();
	
	function __construct(){
		if(	!defined("LOG_LEVEL")||!is_int(LOG_LEVEL)||LOG_LEVEL>LOG_DEBUG||LOG_LEVEL<LOG_EMERG||
			!defined("PROTOCOL_DRIVERS")||!is_array(PROTOCOL_DRIVERS)||
			!defined("MQTT_SERVER_ADDRESS")||
			!defined("MQTT_SERVER_PORT")||!is_int(MQTT_SERVER_PORT)||MQTT_SERVER_PORT<2||MQTT_SERVER_PORT>65535||
			!defined("MQTT_KEEPALIVE_SEC")||!is_int(MQTT_KEEPALIVE_SEC)||MQTT_KEEPALIVE_SEC<1||
			!defined("MQTT_RECONNECT_SEC")||!is_int(MQTT_RECONNECT_SEC)||MQTT_RECONNECT_SEC<1||
			!defined("MIN_BATT_CHARGE_OFFSET")||!is_numeric(MIN_BATT_CHARGE_OFFSET)||MIN_BATT_CHARGE_OFFSET<1||MIN_BATT_CHARGE_OFFSET>99||
			!defined("COMMLOST_TIMEOUT_SEC")||!is_int(COMMLOST_TIMEOUT_SEC)||COMMLOST_TIMEOUT_SEC<1
		){
			throw new Exception("SAUPS: Missing or invalid configuration; ALL the values in config.php MUST be defined and not malformed!");
		}
		
		$this->pses=new Pses(LOG_LEVEL);
		
		$this->startTimestamp=Pses::microtimeInt();
		$this->pses->logWrite(LOG_NOTICE,"sa-ups {} starting",VERSION);
		
		$this->upsData=new SaUpsData();
		
		$this->psesMqtt=new PsesMqtt($this->pses,MQTT_SERVER_ADDRESS.":".MQTT_SERVER_PORT,self::MQTT_TOPICS,array($this,"onMqttConnect"),array($this,"onMqttMessage"),array($this,"onMqttDisconnect"),Pses::secToMicro(MQTT_KEEPALIVE_SEC),Pses::secToMicro(MQTT_RECONNECT_SEC));
		
		foreach(PROTOCOL_DRIVERS as $protocolDriverName){
			if(include_once(dirname(__FILE__)."/ProtocolDrivers/$protocolDriverName/$protocolDriverName.php")){
				try{
					array_push($this->protocolDrivers,(new ReflectionClass($protocolDriverName))->newInstanceArgs(array($this->pses,$this)));
				}catch(Exception $e){
					$this->pses->logWrite(LOG_ERR,"ProtocolDriver [{}] failed to load: [{}]",$protocolDriverName,$e->getMessage());
				}
			}else{
				$this->pses->logWrite(LOG_ERR,"ProtocolDriver [{}] not found",$protocolDriverName);
			}
		}
		
		$this->pses->scheduleRecurrentJob(Pses::secToMicro(COMMLOST_TIMEOUT_SEC/2),array($this,"updateDataCurrent"));
		$this->pses->installShutdownHook(array($this,"onShutdown"));
	}
	
	public function run(){
		$this->pses->run();
	}
	
	public function getUpsStatus(){
		return $this->upsData->upsStatus;
	}
	
	public function getUpsData(){
		$this->upsData->loadFrequency=SaUps::arrayAverage($this->loadFrequency);
		$this->upsData->loadVoltage=SaUps::arrayAverage($this->loadVoltage);
		$this->upsData->batteryCurrent=-array_sum($this->batteryCurrent);
		$this->upsData->inverterTemperature=SaUps::arrayAverage($this->inverterTemperature);
		
		return $this->upsData;
	}
	
	public function onMqttConnect(){
		$this->updateDataCurrent();
	}
	
	public function onMqttMessage($topic,$message){
		$topicArray=explode("/",$topic);
		$topicArrayCount=count($topicArray);
		if($topicArrayCount!==4){
			$this->pses->logWrite(LOG_ERR,"SAUPS_MQTT: topic [{}] had {} fields but 4 were expected!",$topic,$topicArrayCount);
		}else{
			if($topicArray[1]==="total"){
				switch($topicArray[2]){
					case "battery_state_of_charge":
						$this->upsData->batteryStateOfCharge=(float)$message;
						$this->updateBatteryLow();
						break;
					case "battery_temperature":
						$this->upsData->batteryTemperature=(float)$message;
						break;
					case "grid_frequency":
						$this->upsData->gridFrequency=(float)$message;
						$this->updateGridAvailable();
						break;
					case "grid_power":
						$this->upsData->gridPower=(float)$message;
						break;
					case "grid_voltage":
						$this->upsData->gridVoltage=(float)$message;
						if(($this->upsData->gridVoltage>$this->upsData->gridVoltageMax)||($this->upsData->gridVoltageMax===null)){
							$this->upsData->gridVoltageMax=$this->upsData->gridVoltage;
						}
						if(($this->upsData->gridVoltage<$this->upsData->gridVoltageMin)||($this->upsData->gridVoltageMin===null)){
							$this->upsData->gridVoltageMin=$this->upsData->gridVoltage;
						}
						$this->updateGridAvailable();
						break;
					case "load_percentage":
						$this->upsData->loadPercentage=(float)$message;
						break;
					case "load_power":
						$this->upsData->loadPower=(float)$message;
						break;
					default:
						return;
				}
			}else{
				$inverterNumber=explode("_",$topicArray[1])[1];
				switch($topicArray[2]){
					case "ac_output_frequency":
						$this->loadFrequency[$inverterNumber]=$message;
						break;
					case "ac_output_voltage":
						$this->loadVoltage[$inverterNumber]=$message;
						break;
					case "battery_current":
						$this->batteryCurrent[$inverterNumber]=$message;
						break;
					case "battery_float_charge_voltage":
						$this->batteryFloatChargeVoltage[$inverterNumber]=(float)$message;
						$this->upsData->batteryFloatChargeVoltage=SaUps::arrayAverage($this->batteryFloatChargeVoltage);
						$this->updateBatteryPresent();
						break;
					case "battery_voltage":
						$this->batteryVoltage[$inverterNumber]=(float)$message;
						$this->upsData->batteryVoltage=SaUps::arrayAverage($this->batteryVoltage);
						$this->updateBatteryPresent();
						break;
					case "grid_frequency_high":
						$this->transferFrequencyHigh[$inverterNumber]=(float)$message;
						$this->upsData->transferFrequencyHigh=SaUps::arrayAverage($this->transferFrequencyHigh);
						$this->updateGridAvailable();
						break;
					case "grid_frequency_low":
						$this->transferFrequencyLow[$inverterNumber]=(float)$message;
						$this->upsData->transferFrequencyLow=SaUps::arrayAverage($this->transferFrequencyLow);
						$this->updateGridAvailable();
						break;
					case "grid_voltage_high":
						$this->transferVoltageHigh[$inverterNumber]=(float)$message;
						$this->upsData->transferVoltageHigh=SaUps::arrayAverage($this->transferVoltageHigh);
						$this->updateGridAvailable();
						break;
					case "grid_voltage_low":
						$this->transferVoltageLow[$inverterNumber]=(float)$message;
						$this->upsData->transferVoltageLow=SaUps::arrayAverage($this->transferVoltageLow);
						$this->updateGridAvailable();
						break;
					case "output_shutdown_capacity":
						$this->batteryShutdownCapacity[$inverterNumber]=(float)$message;
						$this->upsData->batteryShutdownCapacity=SaUps::arrayAverage($this->batteryShutdownCapacity)+MIN_BATT_CHARGE_OFFSET;
						$this->updateBatteryLow();
						break;
					case "temperature":
						$this->inverterTemperature[$inverterNumber]=$message;
						break;
					default:
						return;
				}
			}
			$this->upsData->upsStatus->lastUpdateTime=Pses::microtimeInt();
		}
	}
	
	public function onMqttDisconnect(){
		$this->updateDataCurrent();
	}
	
	private function updateGridAvailable(){
		if(		$this->upsData->gridVoltage!=null&&
				$this->upsData->gridFrequency!=null&&
				$this->upsData->transferFrequencyHigh!==null&&
				$this->upsData->transferFrequencyLow!==null&&
				$this->upsData->transferVoltageHigh!==null&&
				$this->upsData->transferVoltageLow!==null){
			$gridPreviouslyAvailable=$this->upsData->upsStatus->gridAvailable;
			$voltageAcceptable=		$this->upsData->gridVoltage<=$this->upsData->transferVoltageHigh&&
									$this->upsData->gridVoltage>=$this->upsData->transferVoltageLow;
			$frequencyAcceptable=	$this->upsData->gridFrequency<=$this->upsData->transferFrequencyHigh&&
									$this->upsData->gridFrequency>=$this->upsData->transferFrequencyLow;
			$this->upsData->upsStatus->gridAvailable=$voltageAcceptable&&$frequencyAcceptable;
			if($gridPreviouslyAvailable&&!$this->upsData->upsStatus->gridAvailable){
				$this->pses->logWrite(LOG_NOTICE,"SAUPS: Grid power is no longer available!");
				$this->upsData->transferReason="Unacceptable Utility ".($voltageAcceptable?"Frequency":"Voltage")." Change";
				$this->callDriverMethod("setGridAvailable",false);
			}else if(!$gridPreviouslyAvailable&&$this->upsData->upsStatus->gridAvailable){
				$this->pses->logWrite(LOG_NOTICE,"SAUPS: Grid power is available!");
				$this->callDriverMethod("setGridAvailable",true);
			}
			$this->updateDataCurrent();
		}
	}
	
	private function updateBatteryPresent(){
		if($this->upsData->batteryVoltage!==null&&$this->upsData->batteryFloatChargeVoltage!==null){
			$batteryPreviouslyPresent=$this->upsData->upsStatus->batteryPresent;
			$this->upsData->upsStatus->batteryPresent=$this->upsData->batteryVoltage>$this->upsData->batteryFloatChargeVoltage*0.8;
			if($batteryPreviouslyPresent&&!$this->upsData->upsStatus->batteryPresent){
				$this->pses->logWrite(LOG_NOTICE,"SAUPS: Battery was removed!");
				$this->callDriverMethod("setBatteryPresent",false);
			}else if($batteryPreviouslyPresent===false&&$this->upsData->upsStatus->batteryPresent){
				$this->pses->logWrite(LOG_NOTICE,"SAUPS: Battery was installed!");
				$this->callDriverMethod("setBatteryPresent",true);
			}
			$this->updateDataCurrent();
		}
	}
	
	private function updateBatteryLow(){
		if($this->upsData->batteryStateOfCharge!==null&&$this->upsData->batteryShutdownCapacity!==null){
			$batteryPreviouslyLow=$this->upsData->upsStatus->batteryLow;
			$this->upsData->upsStatus->batteryLow=$this->upsData->batteryStateOfCharge<=$this->upsData->batteryShutdownCapacity;
			if($batteryPreviouslyLow&&!$this->upsData->upsStatus->batteryLow){
				$this->pses->logWrite(LOG_NOTICE,"SAUPS: Battery is no longer low!");
				$this->callDriverMethod("setBatteryLow",false);
			}else if($batteryPreviouslyLow===null&&$this->upsData->upsStatus->batteryLow){
				$this->pses->logWrite(LOG_NOTICE,"SAUPS: Battery is low!");
				$this->callDriverMethod("setBatteryLow",true);
			}
			$this->updateDataCurrent();
		}
	}
	
	public function updateDataCurrent(){
		$dataPreviouslyGood=$this->upsData->upsStatus->dataCurrent;
		$this->upsData->upsStatus->dataCurrent=	$this->psesMqtt->isConnected()&&
												$this->upsData->upsStatus->gridAvailable!==null&&
												$this->upsData->upsStatus->batteryPresent!==null&&
												$this->upsData->upsStatus->batteryLow!==null&&
												$this->upsData->upsStatus->lastUpdateTime+PSES::secToMicro(COMMLOST_TIMEOUT_SEC)>=PSES::microtimeInt();
		if($dataPreviouslyGood&&!$this->upsData->upsStatus->dataCurrent){
			$this->pses->logWrite(LOG_NOTICE,"SAUPS: Data is no longer current!");
			$this->callDriverMethod("setDataCurrent",false);
		}else if(!$dataPreviouslyGood&&$this->upsData->upsStatus->dataCurrent){
			$this->pses->logWrite(LOG_NOTICE,"SAUPS: Data is current!");
			$this->callDriverMethod("setDataCurrent",true);
		}
	}
	
	public function onShutdown(){
		$this->pses->logWrite(LOG_NOTICE,"SAUPS: Shutting down");
		$this->callDriverMethod("shutdown");
		$this->psesMqtt->shutdown();
	}
	
	private function callDriverMethod($methodName,$argument=null){
		foreach($this->protocolDrivers as $protocolDriver){
			if(method_exists($protocolDriver,$methodName)){
				array($protocolDriver,$methodName)($argument);
			}
		}
	}
	
	private static function arrayAverage($array){
		if(count($array)>=1){
			return array_sum($array)/count($array);
		}else{
			return null;
		}
	}
}

$saUps=new SaUps();
$saUps->run();
?>
