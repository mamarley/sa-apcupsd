#!/usr/bin/php
<?php
/*	SaUpsNut.php - sa-ups NUT Protocol driver
	Copyright © 2023 Michael Marley <michael@michaelmarley.com>
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class SaUpsNut{
	private $pses;
	private $saUps;
	private $serverStreamID;
	
	function __construct($pses,$saUps){
		$this->pses=$pses;
		$this->saUps=$saUps;
		
		if(!include_once(dirname(__FILE__)."/config.php")){
			throw new Exception("config.php does not exist!");
		}
		
		if(defined("NUT_LISTEN_ADDRESS")&&defined("NUT_LISTEN_PORT")&&is_int(NUT_LISTEN_PORT)&&NUT_LISTEN_PORT>1024&&NUT_LISTEN_PORT<65535&&
			defined("NUT_INACTIVE_TIMEOUT_SEC")&&is_int(NUT_INACTIVE_TIMEOUT_SEC)&&NUT_INACTIVE_TIMEOUT_SEC>0
		){
			$listenAddress="tcp://".NUT_LISTEN_ADDRESS.":".NUT_LISTEN_PORT;
			$this->serverStreamID=$this->pses->serverSocket($listenAddress,array($this,"onConnect"),array($this,"onDataAvailable"),array($this,"onDisconnect"),Pses::secToMicro(NUT_INACTIVE_TIMEOUT_SEC),array($this,"onPing"));
			$this->pses->logWrite(LOG_INFO,"NUT: {}: listening",$listenAddress);
		}else{
			throw new Exception("Missing or invalid configuration; ALL the values in config.php MUST be defined and not malformed!");
		}
	}
	
	public function onConnect($stream,$context){
		$this->pses->logWrite(LOG_INFO,"NUT: {}: Connection accepted",$context->clientName);
		return true;
	}
	
	public function onDataAvailable($stream,$context){
		$command=explode(" ",trim(fgets($stream)));
		try{
			switch($command[0]){
				case "STARTTLS":
					throw new FeatureNotSupportedException();
					break;
				case "USERNAME":
				case "PASSWORD":
				case "LOGIN":
					$this->send($stream,"OK");
					break;
				case "GET":
					if(isset($command[1])){
						switch($command[1]){
							case "VAR":
								if(isset($command[2])&&isset($command[3])){
									if($command[3]==="ups.status"){
										$this->send($stream,SaUpsNut::formatVar($command[2],$command[3],$this->statusString($this->saUps->getUpsStatus())));
									}else{
										$varArray=$this->buildVarArray();
										if(array_key_exists($command[3],$varArray)){
											$this->send($stream,SaUpsNut::formatVar($command[2],$command[3],$varArray[$command[3]]));
										}else{
											throw new VarNotSupportedException($command[3]);
										}
									}
								}else{
									throw new InvalidArgumentException(implode("",$command));
								}
								break;
							default:
								throw new InvalidArgumentException(implode("",$command));
						}
					}else{
						throw new InvalidArgumentException(implode("",$command));
					}
					break;
				case "LIST":
					if(isset($command[1])&&isset($command[2])){
						switch($command[1]){
							case "VAR":
								$this->sendVarList($stream,$command[2],$this->buildVarArray());
								break;
							default:
								throw new InvalidArgumentException(implode("",$command));
						}
					}else{
						throw new InvalidArgumentException(implode(" ",$command));
					}
					break;
				case "LOGOUT":
					$this->send($stream,"OK Goodbye");
					$this->pses->disconnect($context->streamID);
					break;
				default:
					throw new UnknownCommandException($command[0]);
			}
		}catch(DataStaleException $e){
			$this->send($stream,"ERR DATA-STALE");
		}catch(FeatureNotSupportedException $e){
			$this->send($stream,"ERR FEATURE-NOT-SUPPORTED");
		}catch(InvalidArgumentException $e){
			$this->pses->logWrite(LOG_NOTICE,"NUT: {}: client sent command with invalid argument combination [{}]",$context->clientName,$e->getMessage());
			$this->send($stream,"ERR INVALID-ARGUMENT");
		}catch(UnknownCommandException $e){
			$this->pses->logWrite(LOG_NOTICE,"NUT: {}: client sent unknown command [{}]",$context->clientName,$e->getMessage());
			$this->send($stream,"ERR UNKNOWN-COMMAND");
		}catch(VarNotSupportedException $e){
			$this->pses->logWrite(LOG_NOTICE,"NUT: {}: client requested unsupported variable [{}]",$context->clientName,$e->getMessage());
			$this->send($stream,"ERR VAR-NOT-SUPPORTED");
		}
	}
	
	public function onDisconnect($stream,$context){
		$this->pses->logWrite(LOG_INFO,"NUT: {}: client disconnected",$context->clientName);
	}
	
	public function onPing($stream,$context){
		if($context->lastReceivedTimestamp<=(Pses::microtimeInt()-Pses::secToMicro(NUT_INACTIVE_TIMEOUT_SEC))){
			$this->pses->logWrite(LOG_INFO,"NUT: {}: client has been inactive for {} seconds, disconnecting",$context->clientName,NUT_INACTIVE_TIMEOUT_SEC);
			$this->pses->disconnect($context->streamID);
		}
	}
	
	public function shutdown(){
		$this->pses->disconnect($this->serverStreamID);
	}
	
	private function statusString($upsStatus){
		if($upsStatus->dataCurrent){
			$status="";
			if($upsStatus->gridAvailable){
				$status.="OL";
			}else{
				$status.="OB";
			}
			if($upsStatus->batteryLow){
				$status.=" LB";
			}
			if(!$upsStatus->batteryPresent){
				$status.=" RB";
			}
			return $status;
		}else{
			return "OFF";
		}
	}
	
	private function send($stream,$message){
		fwrite($stream,"$message\n");
	}
	
	private function sendVarList($stream,$upsName,$varArray){
		$message="BEGIN LIST VAR $upsName\n";
		foreach($varArray as $var=>$value){
			$message.=SaUpsNut::formatVar($upsName,$var,$value)."\n";
		}
		$message.="END LIST VAR $upsName";
		$this->send($stream,$message);
	}
	
	private function buildVarArray(){
		$upsData=$this->saUps->getUpsData();
		
		if(!$upsData->upsStatus->dataCurrent){
			throw new DataStaleException();
		}
		
		$response=array();
		$response["battery.charge"]=$upsData->batteryStateOfCharge;
		$response["battery.current"]=$upsData->batteryCurrent;
		$response["battery.temperature"]=$upsData->batteryTemperature;
		$response["battery.voltage"]=$upsData->batteryVoltage;
		$response["device.model"]="sa-ups";
		$response["device.type"]="ups";
		$response["driver.name"]="sa-ups";
		$response["driver.version.internal"]=VERSION;
		$response["input.current"]=SaUpsNut::calculateCurrent($upsData->gridPower,$upsData->gridVoltage);
		$response["input.frequency"]=$upsData->gridFrequency;
		$response["input.frequency.high"]=$upsData->transferFrequencyHigh;
		$response["input.frequency.low"]=$upsData->transferFrequencyLow;
		$response["input.transfer.high"]=$upsData->transferVoltageHigh;
		$response["input.transfer.low"]=$upsData->transferVoltageLow;
		$response["input.transfer.reason"]=$upsData->transferReason;
		$response["input.voltage"]=$upsData->gridVoltage;
		$response["input.voltage.maximum"]=$upsData->gridVoltageMax;
		$response["input.voltage.minimum"]=$upsData->gridVoltageMin;
		$response["output.current"]=SaUpsNut::calculateCurrent($upsData->loadPower,$upsData->loadVoltage);
		$response["output.frequency"]=$upsData->loadFrequency;
		$response["output.voltage"]=$upsData->loadVoltage;
		$response["ups.load"]=$upsData->loadPercentage;
		$response["ups.power"]=$upsData->loadPower;
		$response["ups.status"]=$this->statusString($upsData->upsStatus);
		$response["ups.temperature"]=$upsData->inverterTemperature;
		return $response;
	}
	
	private static function formatVar($upsName,$var,$value){
		return "VAR $upsName $var \"$value\"";
	}
	
	private static function calculateCurrent($power,$voltage){
		if($power===null||$voltage===null){
			return null;
		}else if($voltage===0){
			return 0;
		}else{
			return round($power/$voltage,2);
		}
	}
}

class DataStaleException extends Exception{}
class FeatureNotSupportedException extends Exception{}
class UnknownCommandException extends Exception{}
class VarNotSupportedException extends Exception{}
?>
