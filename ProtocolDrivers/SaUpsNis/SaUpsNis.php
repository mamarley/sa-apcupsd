#!/usr/bin/php
<?php
/*	SaUpsNis.php - sa-ups APCUPSD NIS Protocol Driver
	Copyright © 2023 Michael Marley <michael@michaelmarley.com>
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class NisConnectionState{
	public $length=null;
	public $command="";
}

class SaUpsNis{
	private const DATE_FORMAT="Y-m-d H:i:s O";
	
	private const STATFLAG_UPS_ONLINE=0x00000008;
	private const STATFLAG_UPS_ONBATT=0x00000010;
	private const STATFLAG_UPS_BATTLOW=0x00000040;
	private const STATFLAG_UPS_COMMLOST=0x00000100;
	private const STATFLAG_UPS_SHUTDOWN=0x00000200;
	private const STATFLAG_UPS_FASTPOLL=0x00040000;
	private const STATFLAG_UPS_SHUT_EMERG=0x00400000;
	private const STATFLAG_UPS_PLUGGED=0x01000000;
	private const STATFLAG_UPS_BATTPRESENT=0x04000000;
	
	private $pses;
	private $saUps;
	private $serverStreamID;
	private $startTimestamp;
	
	function __construct($pses,$saUps){
		$this->pses=$pses;
		$this->saUps=$saUps;
		
		if(!include_once(dirname(__FILE__)."/config.php")){
			throw new Exception("config.php does not exist!");
		}
		
		if(defined("NIS_LISTEN_ADDRESS")&&defined("NIS_LISTEN_PORT")&&is_int(NIS_LISTEN_PORT)&&NIS_LISTEN_PORT>1024&&NIS_LISTEN_PORT<65535&&
			defined("NIS_INACTIVE_TIMEOUT_SEC")||is_int(NIS_INACTIVE_TIMEOUT_SEC)||NIS_INACTIVE_TIMEOUT_SEC>0
		){
			$listenAddress="tcp://".NIS_LISTEN_ADDRESS.":".NIS_LISTEN_PORT;
			$this->startTimestamp=Pses::microtimeInt();
			$this->serverStreamID=$this->pses->serverSocket($listenAddress,array($this,"onConnect"),array($this,"onDataAvailable"),array($this,"onDisconnect"),Pses::secToMicro(NIS_INACTIVE_TIMEOUT_SEC),array($this,"onPing"));
			$this->pses->logWrite(LOG_INFO,"NIS: {}: listening",$listenAddress);
		}else{
			throw new Exception("Missing or invalid configuration; ALL the values in config.php MUST be defined and not malformed!");
		}
	}
	
	public function onConnect($stream,&$context){
		$context->state=new NisConnectionState();
		$this->pses->logWrite(LOG_INFO,"NIS: {}: Connection accepted",$context->clientName);
		return true;
	}
	
	public function onDataAvailable($stream,&$context){
		$state=$context->state;
		$data=fgets($stream);
		if($state->length===null){
			if(strlen($data)>=2){
				$state->length=hexdec(bin2hex(substr($data,0,2)));
				$state->command.=substr($data,2);
			}
		}else{
			$state->command.=$data;
		}
		if(strlen($state->command)===$state->length){
			switch($state->command){
				case "status":
					$upsData=$this->saUps->getUpsData();
					$response=array();
					$response["DATE"]=SaUpsNis::formatDate($upsData->upsStatus->lastUpdateTime);
					$response["HOSTNAME"]=gethostname();
					$response["VERSION"]=VERSION;
					$response["UPSNAME"]="Solar-Assistant_Virtual_UPS";
					$response["CABLE"]="Network";
					$response["DRIVER"]="sa-ups";
					$response["UPSMODE"]="Stand Alone";
					$response["STARTTIME"]=SaUpsNis::formatDate($this->startTimestamp);
					$response["MODEL"]="Solar-Assistant_Virtual_UPS";
					$response["STATUS"]=$this->statusString($upsData->upsStatus);
					$response["LINEV"]=SaUpsNis::formatFloat($upsData->gridVoltage)." Volts";
					$response["LOADPCT"]=SaUpsNis::formatFloat($upsData->loadPercentage)." Percent";
					$response["BCHARGE"]=SaUpsNis::formatFloat($upsData->batteryStateOfCharge)." Percent";
					$response["MBATCHG"]=SaUpsNis::formatFloat($upsData->batteryShutdownCapacity)." Percent";
					$response["MAXLINEV"]=SaUpsNis::formatFloat($upsData->gridVoltageMax)." Volts";
					$response["MINLINEV"]=SaUpsNis::formatFloat($upsData->gridVoltageMin)." Volts";
					$response["OUTPUTV"]=SaUpsNis::formatFloat($upsData->loadVoltage)." Volts";
					$response["LOTRANS"]=SaUpsNis::formatFloat($upsData->transferVoltageLow)." Volts";
					$response["HITRANS"]=SaUpsNis::formatFloat($upsData->transferVoltageHigh)." Volts";
					$response["ITEMP"]=SaUpsNis::formatFloat($upsData->inverterTemperature)." C";
					$response["BATTV"]=SaUpsNis::formatFloat($upsData->batteryVoltage)." Volts";
					$response["LINEFREQ"]=SaUpsNis::formatFloat($upsData->gridFrequency)." Hz";
					$response["LASTXFER"]=($upsData->transferReason!==null?$upsData->transferReason:"N/A");
					$response["STATFLAG"]=$this->statFlag($upsData->upsStatus);
					$this->sendArray($stream,$response);
					break;
				case "events":
					$this->send($stream,"");
					break;
				default:
					$this->pses->logWrite(LOG_NOTICE,"NIS: {}: client sent invalid command [{}]",$context->clientName,$state->command);
					$this->send($stream,"Invalid command");
			}
			$context->state=new NisConnectionState();
		}else if(strlen($state->command)>=$state->length){
			$this->pses->logWrite(LOG_NOTICE,"NIS: {}: client sent command [{}] that did not have expected length [{}]",$context->clientName,$state->command,$state->length);
			$this->send($stream,"Invalid command");
			$context->state=new NisConnectionState();
		}
	}
	
	public function onDisconnect($stream,$context){
		$this->pses->logWrite(LOG_INFO,"NIS: {}: client disconnected",$context->clientName);
	}
	
	public function onPing($stream,$context){
		if($context->lastReceivedTimestamp<=(Pses::microtimeInt()-Pses::secToMicro(NIS_INACTIVE_TIMEOUT_SEC))){
			$this->pses->logWrite(LOG_INFO,"NIS: {}: client has been inactive for {} seconds, disconnecting",$context->clientName,NIS_INACTIVE_TIMEOUT_SEC);
			$this->pses->disconnect($context->streamID);
		}
	}
	
	public function shutdown(){
		$this->pses->disconnect($this->serverStreamID);
	}
	
	private function send($stream,$message){
		fwrite($stream,SaUpsNis::createMessage($message)."\0\0");
	}
	
	private function sendArray($stream,$array){
		$message="";
		foreach($array as $key=>$value){
			$message.=SaUpsNis::createMessage(SaUpsNis::formatLine($key,$value));
		}
		$message.=SaUpsNis::createMessage(SaUpsNis::formatLine("END APC",date(self::DATE_FORMAT)));
		$message.="\0\0";
		$message=SaUpsNis::createMessage(SaUpsNis::formatLine("APC",sprintf("001,%03d,%04d",count($array)+1,strlen($message)))).$message;
		fwrite($stream,$message);
	}
	
	private function statFlag($upsStatus){
		$statflag=0;
		
		if($upsStatus->dataCurrent){
			if($upsStatus->gridAvailable){
				$statflag|=self::STATFLAG_UPS_ONLINE;
			}else{
				$statflag|=self::STATFLAG_UPS_ONBATT;
				$statflag|=self::STATFLAG_UPS_FASTPOLL;
			}
			
			if($upsStatus->batteryLow){
				$statflag|=self::STATFLAG_UPS_BATTLOW;
				if(!$upsStatus->gridAvailable){
					$statflag|=self::STATFLAG_UPS_SHUTDOWN;
					$statflag|=self::STATFLAG_UPS_SHUT_EMERG;
				}
			}
			
			if($upsStatus->batteryPresent){
				$statflag|=self::STATFLAG_UPS_BATTPRESENT;
			}
		}else{
			$statflag|=self::STATFLAG_UPS_COMMLOST;
		}
		
		$statflag|=self::STATFLAG_UPS_PLUGGED;
		
		return sprintf("0x%08X",$statflag);
	}
	
	private function statusString($upsStatus){
		if($upsStatus->dataCurrent){
			$status="";
			if($upsStatus->gridAvailable){
				$status.="ONLINE";
			}else{
				$status.="ONBATT";
			}
			if($upsStatus->batteryLow){
				$status.=" LOWBATT";
			}
			if(!$upsStatus->batteryPresent){
				$status.=" NOBATT";
			}
			return $status;
		}else{
			return "COMMLOST";
		}
	}
	
	private static function formatDate($timestamp){
		if($timestamp!==null){
			$dateTime=DateTime::createFromFormat("U.u",number_format(Pses::microToSec($timestamp),6,".",""));
			$dateTime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			return $dateTime->format(self::DATE_FORMAT);
		}else{
			return "N/A";
		}
	}
	
	private static function formatFloat($number){
		return sprintf("%.1f",$number!==null?round($number,1):0);
	}
	
	private static function formatLine($key,$value){
		return sprintf("%-9s: %s",$key,$value);
	}
	
	private static function createMessage($message){
		return hex2bin(sprintf("%04X",strlen($message)+1)).$message."\n";
	}
}
?>
