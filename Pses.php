<?php
/*	Pses.php - PHP Simple Event-Driven Sockets Library
	Copyright © 2023 Michael Marley <michael@michaelmarley.com>
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class PsesStreamMetadata{
	public $type;
	public $onConnect;
	public $onDataAvailable;
	public $onDisconnect;
	public $pingInterval;
	public $onPing;
	public $pingTimerID;
	public $listenStreamID;
	public $serverStreams;
	public $context;
	
	function __construct(){
		$this->context=new PsesStreamContext();
		$this->serverStreams=array();
	}
}

class PsesStreamContext{
	public $streamID;
	public $clientName;
	public $lastReceivedTimestamp;
	public $state;
}

class PsesTimer{
	public $timerID;
	public $callback;
	public $arguments;
	
	function __construct($timerID,$callback,$arguments){
		$this->timerID=$timerID;
		$this->callback=$callback;
		$this->arguments=$arguments;
	}
}

class Pses{
	private const STREAM_TYPE_LISTEN=0;
	private const STREAM_TYPE_SERVER=1;
	private const STREAM_TYPE_CLIENT=2;
	
	private $running=false;
	private $logLevel;
	private $interactiveShell;
	private $justReceivedSignal=false;
	
	private $streams=array();
	private $connectingClientStreams=array();
	
	private $streamMetadata=array();
	
	private $timerIDs=array();
	private $timerTimestamps=array();
	
	private $recurrentJobIDs=array();
	
	private $reloadHooks=array();
	private $shutdownHooks=array();
	
	function __construct($logLevel=LOG_NOTICE){
		$this->logLevel=$logLevel;
		$this->interactiveShell=posix_isatty(STDOUT);
		if(!$this->interactiveShell){
			openlog(basename($_SERVER["PHP_SELF"]),0,LOG_DAEMON);
		}
		$signalHandler=array($this,"handleSignal");
		pcntl_signal(SIGHUP,$signalHandler);
		pcntl_signal(SIGINT,$signalHandler);
		pcntl_signal(SIGTERM,$signalHandler);
	}
	
	public function serverSocket($address,$onConnect,$onDataAvailable,$onDisconnect,$pingInterval=null,$onPing=null){
		if(!is_callable($onConnect)){
			throw new Exception("onConnect callback must be defined!");
		}
		if(!is_callable($onDataAvailable)){
			throw new Exception("onDataAvailable callback must be defined!");
		}
		if(!is_callable($onDisconnect)){
			throw new Exception("onDisconnect callback must be defined!");
		}
		if($pingInterval!==null&&!Pses::pingIntervalIsValid($pingInterval)){
			throw new Exception("pingInterval must be positive integer!");
		}else if($pingInterval!==null&&!is_callable($onPing)){
			throw new Exception("onPing callback must be defined if pingInterval is set!");
		}
		
		$stream=stream_socket_server($address,$errorCode,$errorMessage);
		if(!$stream){
			throw new Exception("Failed to open server socket: $errorCode: $errorMessage");
		}
		$this->logWrite(LOG_INFO,"PSES: {}: server socket listening",$address);
		
		$streamID=Pses::uuid($this->streamMetadata);
		$streamMetadata=new PsesStreamMetadata();
		$streamMetadata->type=self::STREAM_TYPE_LISTEN;
		$streamMetadata->onConnect=$onConnect;
		$streamMetadata->onDataAvailable=$onDataAvailable;
		$streamMetadata->onDisconnect=$onDisconnect;
		$streamMetadata->pingInterval=$pingInterval;
		$streamMetadata->onPing=$onPing;
		$streamMetadata->context->streamID=$streamID;
		$streamMetadata->context->clientName=$address;
		
		$this->streams[$streamID]=$stream;
		$this->streamMetadata[$streamID]=$streamMetadata;
		return $streamID;
	}
	
	public function getStream($streamID){
		return $this->streams[$streamID];
	}
	
	public function getServerStreams($listenStreamID){
		return $this->streamMetadata[$listenStreamID]->serverStreams;
	}
	
	public function clientSocket($address,$onConnect,$onDataAvailable,$onDisconnect,$pingInterval=null,$onPing=null){
		if(!is_callable($onConnect)){
			throw new Exception("onConnect callback must be defined!");
		}
		if(!is_callable($onDataAvailable)){
			throw new Exception("onDataAvailable callback must be defined!");
		}
		if(!is_callable($onDisconnect)){
			throw new Exception("onDisconnect callback must be defined!");
		}
		if($pingInterval!==null&&!Pses::pingIntervalIsValid($pingInterval)){
			throw new Exception("pingInterval must be positive integer!");
		}else if($pingInterval!==null&&!is_callable($onPing)){
			throw new Exception("onPing callback must be defined if pingInterval is set!");
		}
		
		$stream=@stream_socket_client($address,$errorCode,$errorMessage,null,STREAM_CLIENT_CONNECT|STREAM_CLIENT_ASYNC_CONNECT);
		if(!$stream){
			throw new Exception("Failed to open client socket: $errorCode: $errorMessage");
		}
		stream_set_blocking($stream,false);
		$this->logWrite(LOG_INFO,"PSES: {}: client socket connecting",$address);
		
		$streamID=Pses::uuid($this->streamMetadata);
		$streamMetadata=new PsesStreamMetadata();
		$streamMetadata->type=self::STREAM_TYPE_CLIENT;
		$streamMetadata->onConnect=$onConnect;
		$streamMetadata->onDataAvailable=$onDataAvailable;
		$streamMetadata->onDisconnect=$onDisconnect;
		$streamMetadata->pingInterval=$pingInterval;
		$streamMetadata->onPing=$onPing;
		$streamMetadata->context->streamID=$streamID;
		
		$this->connectingClientStreams[$streamID]=$stream;
		$this->streamMetadata[$streamID]=$streamMetadata;
		return $streamID;
	}
	
	public function disconnect($streamID){
		if(array_key_exists($streamID,$this->streams)){
			$stream=$this->streams[$streamID];
			$streamMetadata=$this->streamMetadata[$streamID];
			fclose($stream);
			if($streamMetadata->type===self::STREAM_TYPE_LISTEN){
				foreach($streamMetadata->serverStreams as $serverStreamID=>$serverStream){
					$this->disconnect($serverStreamID);
				}
				$this->logWrite(LOG_INFO,"PSES: {}: listen socket closed",$streamMetadata->context->clientName);
			}else if($streamMetadata->type===self::STREAM_TYPE_SERVER||$streamMetadata->type===self::STREAM_TYPE_CLIENT){
				if($streamMetadata->pingInterval!==null){
					$this->cancelJob($streamMetadata->pingTimerID);
				}
				($streamMetadata->onDisconnect)($stream,$streamMetadata->context);
				unset($this->streamMetadata[$streamMetadata->listenStreamID]->serverStreams[$streamID]);
				$this->logWrite(LOG_INFO,"PSES: {}: {} connection closed by local host",$streamMetadata->context->clientName,fn()=>$streamMetadata->type===self::STREAM_TYPE_SERVER?"server":"client");
			}
			unset($this->streams[$streamID]);
			unset($this->streamMetadata[$streamID]);
		}else{
			throw new Exception("Cannot disconnect nonexistent stream $streamID");
		}
	}
	
	public function scheduleJob($timestamp,$callable,$arguments=array()){
		if(!is_int($timestamp)){
			throw new Exception("Invalid timestamp [$timestamp]!");
		}
		if(!is_callable($callable)){
			throw new Exception("Callable must be passed!");
		}
		$timerID=Pses::uuid($this->timerIDs);
		$this->timerIDs[$timerID]=$timestamp;
		if(!array_key_exists($timestamp,$this->timerTimestamps)){
			$this->timerTimestamps[$timestamp]=array();
		}
		$this->timerTimestamps[$timestamp][]=new PsesTimer($timerID,$callable,$arguments);
		ksort($this->timerTimestamps,SORT_NUMERIC);
		$this->logWrite(LOG_DEBUG,"PSES: task {} with ID {} scheduled to run at {}",fn()=>Pses::formatCallable($callable),$timerID,fn()=>Pses::formatMicrotime($timestamp));
		return $timerID;
	}
	
	public function cancelJob($timerID){
		if(array_key_exists($timerID,$this->timerIDs)){
			$timestamp=$this->timerIDs[$timerID];
			foreach($this->timerTimestamps[$timestamp] as $index=>$psesTimer){
				if($psesTimer->timerID===$timerID){
					unset($this->timerTimestamps[$timestamp][$index]);
					if(count($this->timerTimestamps[$timestamp])===0){
						unset($this->timerTimestamps[$timestamp]);
					}
					unset($this->timerIDs[$timerID]);
					$this->logWrite(LOG_DEBUG,"PSES: canceled task {} with ID {} scheduled to run at {}",fn()=>Pses::formatCallable($psesTimer->callback),$timerID,fn()=>Pses::formatMicrotime($timestamp));
					break;
				}
			}
		}else{
			$this->logWrite(LOG_DEBUG,"PSES: task with ID {} could not be canceled as it does not exist",$timerID);
		}
	}
	
	public function scheduleRecurrentJob($interval,$callable,$arguments=array()){
		if(!is_int($interval)||$interval<1){
			throw new Exception("Interval must be positive integer!");
		}
		if(!is_callable($callable)){
			throw new Exception("Callable must be passed!");
		}
		$recurrentJobID=Pses::uuid($this->recurrentJobIDs);
		$this->logWrite(LOG_DEBUG,"PSES: recurrent task {} with ID {} scheduled to run every {}μs",fn()=>Pses::formatCallable($callable),$recurrentJobID,$interval);
		$this->recurrentJobIDs[$recurrentJobID]=$this->scheduleJob(Pses::microtimeInt()+$interval,array($this,"runRecurrentJob"),array($recurrentJobID,$interval,$callable,$arguments));
		return $recurrentJobID;
	}
	
	public function cancelRecurrentJob($recurrentJobID){
		if(array_key_exists($recurrentJobID,$this->recurrentJobIDs)){
			$timerID=$this->recurrentJobIDs[$recurrentJobID];
			if(array_key_exists($timerID,$this->timerIDs)){
				cancelJob($timerID);
				$this->logWrite(LOG_DEBUG,"PSES: canceled recurrent task with ID {}",$recurrentJobID);
			}else{
				$this->logWrite(LOG_DEBUG,"PSES: task with ID {} created from recurrent task with ID {} could not be canceled as it does not exist",$timerID,$recurrentJobID);
			}
			unset($this->recurrentJobIDs[$recurrentJobID]);
		}else{
			$this->logWrite(LOG_DEBUG,"PSES: recurrent task with ID {} could not be canceled as it does not exist",$recurrentJobID);
		}
	}
	
	public function installReloadHook($callable){
		return $this->installHook($this->reloadHooks,$callable);
	}
	
	public function uninstallReloadHook($hookID){
		$this->uninstallHook($this->reloadHooks,$hookID);
	}
	
	public function installShutdownHook($callable){
		return $this->installHook($this->shutdownHooks,$callable);
	}
	
	public function uninstallShutdownHook($hookID){
		$this->uninstallHook($this->shutdownHooks,$hookID);
	}
	
	public function run(){
		if($this->running){
			throw new Exception("Already running!");
		}
		$this->running=true;
		
		while(true){
			$readStreams=$this->streams;
			$writeStreams=$this->connectingClientStreams;
			$nextWakeupDelay=$this->nextWakeup();
			
			if(count($readStreams)>0||count($writeStreams)>0){
				@stream_select($readStreams,$writeStreams,$null,$nextWakeupDelay===null?null:0,$nextWakeupDelay);
			}else if($nextWakeupDelay!==null){
				usleep($nextWakeupDelay);
			}else{
				throw new Exception("No streams and no timer callbacks configured!");
			}
			
			pcntl_signal_dispatch();
			if($this->justReceivedSignal){
				$this->justReceivedSignal=false;
				continue;
			}
			
			$currentTime=Pses::microtimeInt();
			foreach($this->timerTimestamps as $timestamp=>$psesTimerArray){
				if($timestamp<=$currentTime){
					foreach($psesTimerArray as $psesTimer){
						$this->logWrite(LOG_DEBUG,"PSES: running scheduled task {}",fn()=>Pses::formatCallable($psesTimer->callback));
						call_user_func_array($psesTimer->callback,$psesTimer->arguments);
						unset($this->timerIDs[$psesTimer->timerID]);
					}
					unset($this->timerTimestamps[$timestamp]);
				}else{
					break;
				}
			}
			
			foreach($readStreams as $streamID=>$stream){
				$streamMetadata=$this->streamMetadata[$streamID];
				if($streamMetadata->type===self::STREAM_TYPE_LISTEN){
					while($serverStream=@stream_socket_accept($stream,0,$clientName)){
						$currentTime=Pses::microtimeInt();
						stream_set_blocking($serverStream,false);
						$serverStreamID=$this->uuid($this->streamMetadata);
						$serverStreamMetadata=new PsesStreamMetadata();
						$serverStreamMetadata->type=self::STREAM_TYPE_SERVER;
						$serverStreamMetadata->onDataAvailable=$streamMetadata->onDataAvailable;
						$serverStreamMetadata->onDisconnect=$streamMetadata->onDisconnect;
						$serverStreamMetadata->pingInterval=$streamMetadata->pingInterval;
						$serverStreamMetadata->onPing=$streamMetadata->onPing;
						$serverStreamMetadata->lastPingTimestamp=$currentTime;
						$serverStreamMetadata->listenStreamID=$streamID;
						$serverStreamMetadata->context->streamID=$serverStreamID;
						$serverStreamMetadata->context->clientName=$clientName;
						$serverStreamMetadata->context->lastReceivedTimestamp=$currentTime;
						if(($streamMetadata->onConnect)($stream,$serverStreamMetadata->context)){
							$streamMetadata->serverStreams[$serverStreamID]=$serverStream;
							$this->streams[$serverStreamID]=$serverStream;
							$this->streamMetadata[$serverStreamID]=$serverStreamMetadata;
							$this->logWrite(LOG_INFO,"PSES: {}: Connection accepted",$clientName);
							if($serverStreamMetadata->pingInterval!==null){
								$serverStreamMetadata->pingTimerID=$this->scheduleJob($serverStreamMetadata->lastPingTimestamp+$serverStreamMetadata->pingInterval,array($this,"onPing"),array($serverStreamID));
							}
						}else{
							$serverStreamMetadata->onDisconnect($stream,$serverStreamMetadata);
							fclose($serverStream);
							$this->logWrite(LOG_INFO,"PSES: {}: Connection refused by onConnect returning falsy",$clientName);
						}
					}
				}else{
					$peekedByte=stream_socket_recvfrom($stream,1,STREAM_PEEK);
					if(feof($stream)||($peekedByte!==false&&strlen($peekedByte)===0)){
						($streamMetadata->onDisconnect)($stream,$streamMetadata->context);
						$this->logWrite(LOG_INFO,"PSES: {}: server connection closed by remote host",$streamMetadata->context->clientName);
						if($streamMetadata->pingInterval!==null){
							$this->cancelJob($streamMetadata->pingTimerID);
						}
						unset($this->streamMetadata[$this->streamMetadata[$streamID]->listenStreamID]->serverStreams[$streamID]);
						unset($this->streams[$streamID]);
						unset($this->streamMetadata[$streamID]);
						fclose($stream);
					}else{
						$streamMetadata->context->lastReceivedTimestamp=Pses::microtimeInt();
						($streamMetadata->onDataAvailable)($stream,$streamMetadata->context);
					}
				}
			}
			
			foreach($writeStreams as $streamID=>$stream){
				$currentTime=Pses::microtimeInt();
				$streamMetadata=$this->streamMetadata[$streamID];
				$clientName=stream_socket_get_name($stream,true);
				$streamMetadata->lastPingTimestamp=$currentTime;
				$streamMetadata->context->clientName=$clientName;
				$streamMetadata->context->lastReceivedTimestamp=$currentTime;
				if($clientName&&($streamMetadata->onConnect)($stream,$streamMetadata->context)){
					$this->logWrite(LOG_INFO,"PSES: {}: client connected",$clientName);
					$this->streams[$streamID]=$this->connectingClientStreams[$streamID];
					$streamMetadata->pingTimerID=$this->scheduleJob($streamMetadata->lastPingTimestamp+$streamMetadata->pingInterval,array($this,"onPing"),array($streamID));
					unset($this->connectingClientStreams[$streamID]);
				}else{
					$this->logWrite(LOG_ERR,"PSES: client socket failed to connect!");
					($streamMetadata->onDisconnect)($stream,$streamMetadata->context);
					unset($this->connectingClientStreams[$streamID]);
					unset($this->streamMetadata[$streamID]);
					fclose($stream);
				}
			}
		}
	}
	
	private function nextWakeup(){
		if(count($this->timerTimestamps)>0){
			reset($this->timerTimestamps);
			$nextWakeupTimestamp=key($this->timerTimestamps);
			$nextWakeupDelay=max($nextWakeupTimestamp-Pses::microtimeInt(),0);
			$this->logWrite(LOG_DEBUG,"PSES: scheduling wakeup in {} seconds to run {}",fn()=>Pses::microToSec($nextWakeupDelay),fn()=>Pses::formatCallable($this->timerTimestamps[$nextWakeupTimestamp][0]->callback));
			return $nextWakeupDelay;
		}else{
			$this->logWrite(LOG_DEBUG,"PSES: scheduling wakeup in never");
			return null;
		}
	}
	
	private function onPing($streamID){
		if(array_key_exists($streamID,$this->streams)){
			$this->logWrite(LOG_DEBUG,"PSES: ping timer expired for stream {}",$streamID);
			$stream=$this->streams[$streamID];
			$streamMetadata=$this->streamMetadata[$streamID];
			$streamMetadata->lastPingTimestamp=Pses::microtimeInt();
			$streamMetadata->pingTimerID=$this->scheduleJob($streamMetadata->lastPingTimestamp+$streamMetadata->pingInterval,array($this,"onPing"),array($streamID));
			($streamMetadata->onPing)($stream,$streamMetadata->context);
		}else{
			$this->logWrite(LOG_DEBUG,"PSES: ping timer expired for stream {} but the stream no longer exists",$streamID);
		}
	}
	
	private function runRecurrentJob($recurrentJobID,$interval,$callable,$arguments){
		$this->recurrentJobIDs[$recurrentJobID]=$this->scheduleJob(Pses::microtimeInt()+$interval,array($this,"runRecurrentJob"),array($recurrentJobID,$interval,$callable,$arguments));
		call_user_func_array($callable,$arguments);
	}
	
	private function handleSignal($signal){
		$this->justReceivedSignal=true;
		$this->logWrite(LOG_NOTICE,"PSES: caught signal {}",$signal);
		if($signal===SIGHUP){
			Pses::executeHooks($this->reloadHooks);
		}else if($signal===SIGINT||$signal===SIGTERM){
			Pses::executeHooks($this->shutdownHooks);
			$this->logWrite(LOG_NOTICE,"PSES: Shutting down",$signal);
			exit();
		}
	}
	
	private function executeHooks($hookArray){
		foreach($hookArray as $hookID=>$hook){
			$this->logWrite(LOG_DEBUG,"PSES: executing signal hook {}",fn()=>Pses::formatCallable($hook));
			$hook();
		}
	}
	
	private function installHook(&$hookArray,$callable){
		if(!is_callable($callable)){
			throw new Exception("Callable must be passed!");
		}
		$hookID=Pses::uuid($hookArray);
		$hookArray[$hookID]=$callable;
		$this->logWrite(LOG_DEBUG,"PSES: hook {} with ID {} installed",fn()=>Pses::formatCallable($callable),$hookID);
		return $hookID;
	}
	
	private function uninstallHook(&$hookArray,$hookID){
		if(array_key_exists($hookID,$hookArray)){
			unset($hookArray[$timestamp]);
		}else{
			$this->logWrite(LOG_DEBUG,"PSES: hook with ID {} could not be uninstalled as it does not exist",$hookID);
		}
	}
	
	public function logWrite($priority,$message,...$substitutionValues){
		if($priority<=$this->logLevel){
			foreach($substitutionValues as $substitutionValue){
				if($substitutionValue instanceof \Closure){
					$substitutionValue=$substitutionValue();
				}
				$message=preg_replace("/{}/",$substitutionValue,$message,1);
			}
			if($this->interactiveShell){
				fwrite($priority<=LOG_WARNING?STDERR:STDOUT,$message.PHP_EOL);
			}else{
				syslog($priority,$message);
			}
		}
	}
	
	public static function microtimeInt(){
		return (int)Pses::secToMicro(microtime(true));
	}
	
	public static function secToMicro($seconds){
		return $seconds*1000000;
	}
	
	public static function microToSec($seconds){
		return $seconds/1000000;
	}
	
	public static function formatMicrotime($timestamp){
		$dateTime=DateTime::createFromFormat("U.u",number_format(Pses::microToSec($timestamp),6,".",""));
		if($dateTime){
			$dateTime->setTimeZone(new DateTimeZone(date_default_timezone_get()));
			return $dateTime->format("m-d-Y H:i:s.u");
		}else{
			return null;
		}
	}
	
	public static function pingIntervalIsValid($pingInterval){
		return is_int($pingInterval)&&$pingInterval>0;
	}
	
	private static function formatCallable($callable){
		if(is_array($callable)&&count($callable)>1){
			return get_class($callable[0]).".".$callable[1];
		}else if($callable instanceof \Closure){
			return (new \ReflectionFunction($callable))->getClosureScopeClass()->getName().":Closure";
		}else{
			return $callable;
		}
	}
	
	private static function uuid($array=null){
		do{
			$uuid=vsprintf('%s%s-%s-%s-%s-%s%s%s',str_split(bin2hex(random_bytes(16)),4));
		}while($array!==null&&array_key_exists($uuid,$array));
		return $uuid;
	}
}
?>
