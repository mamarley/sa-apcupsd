<?php
/*	PsesMqtt.php - PHP Simple Event-Driven Sockets MQTT Library
	Copyright © 2023 Michael Marley <michael@michaelmarley.com>
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * TODO:
 * Detect status of MQTT subscribe?
*/

class MqttReceiveState{
	public $command=null;
	public $multiplier=1;
	public $length=0;
	public $lengthCalculated=false;
	public $string="";
}

class PsesMqtt{
	private const KNOWN_COMMANDS=[
		1=>"CONNECT",
		2=>"CONNACK",
		3=>"PUBLISH",
		4=>"PUBACK",
		5=>"PUBREC",
		6=>"PUBREL",
		7=>"PUBCOMP",
		8=>"SUBSCRIBE",
		9=>"SUBACK",
		10=>"UNSUBSCRIBE",
		11=>"UNSUBACK",
		12=>"PINGREQ",
		13=>"PINGRESP",
		14=>"DISCONNECT"
	];
	
	private $pses;
	private $serverAddress;
	private $topics;
	private $onConnect;
	private $onMessage;
	private $onDisconnect;
	private $pingInterval;
	private $reconnectInterval;
	private $running=true;
	
	private $clientID;
	private $messageID=1;
	private $receiveState;
	private $stream=null;
	private $streamID;
	
	function __construct($pses,$serverAddress,$topics,$onConnect,$onMessage,$onDisconnect,$pingInterval,$reconnectInterval){
		if(!is_callable($onConnect)){
			throw new Exception("onConnect callback must be defined!");
		}
		if(!is_callable($onMessage)){
			throw new Exception("onMessage callback must be defined!");
		}
		if(!is_callable($onDisconnect)){
			throw new Exception("onDisconnect callback must be defined!");
		}
		if($pingInterval!==null&&!Pses::pingIntervalIsValid($pingInterval)){
			throw new Exception("pingInterval must be positive integer!");
		}
		if($reconnectInterval!==null&&!Pses::pingIntervalIsValid($reconnectInterval)){
			throw new Exception("reconnectInterval must be positive integer!");
		}
		
		$this->pses=$pses;
		$this->serverAddress=$serverAddress;
		$this->topics=$topics;
		$this->onConnect=$onConnect;
		$this->onMessage=$onMessage;
		$this->onDisconnect=$onDisconnect;
		$this->pingInterval=$pingInterval;
		$this->reconnectInterval=$reconnectInterval;
		$this->clientID="ClientID".rand();
		
		$this->connect();
	}
	
	public function connect(){
		$this->pses->logWrite(LOG_INFO,"MQTT: {}: connecting",$this->serverAddress);
		try{
			$this->streamID=$this->pses->clientSocket("tcp://".$this->serverAddress,array($this,"onConnect"),array($this,"onDataAvailable"),array($this,"onDisconnect"),$this->pingInterval,array($this,"onPing"));
		}catch(Exception $e){
			$this->pses->logWrite(LOG_ERR,"MQTT: {}: connection failed: {}, reconnecting in {} seconds",$this->serverAddress,$e->getMessage(),fn()=>Pses::microToSec($this->reconnectInterval));
			$this->pses->scheduleJob(Pses::microtimeInt()+$this->reconnectInterval,array($this,"connect"));
		}
	}
	
	public function isConnected(){
		return $this->stream!==null;
	}
	
	public function publish($topic,$content){
		$i=0;
		$buffer="";
		
		$buffer.=PsesMqtt::strWriteString($topic,$i);
		
		$buffer.=$content;
		$i+=strlen($content);
		
		$head=" ";
		$cmd=0x30;
		
		$head[0]=chr($cmd);
		$head.=PsesMqtt::setMessageLength($i);
		
		fwrite($this->stream,$head,strlen($head));
		$this->mqttFwrite($this->stream,$buffer);
	}
	
	public function onConnect($stream,$context){
		$this->pses->logWrite(LOG_INFO,"MQTT: {}: negotiating",$this->serverAddress);
		
		$i=0;
		$buffer="";
		
		$buffer.=chr(0x00);
		$i++; // Length MSB
		$buffer.=chr(0x04);
		$i++; // Length LSB
		$buffer.=chr(0x4d);
		$i++; // M
		$buffer.=chr(0x51);
		$i++; // Q
		$buffer.=chr(0x54);
		$i++; // T
		$buffer.=chr(0x54);
		$i++; // T
		$buffer.=chr(0x04);
		$i++; // Protocol Level
		
		//No Will
		$var=0;
		$var+=2;
		
		$buffer.=chr($var);
		$i++;
		
		//Keep alive
		$buffer.=chr($this->pingInterval>>8);
		$i++;
		$buffer.=chr($this->pingInterval&0xff);
		$i++;
		
		$buffer.=PsesMqtt::strWriteString($this->clientID,$i);
		
		$head=chr(0x10);
		
		while($i>0){
			$encodedByte=$i%128;
			$i/=128;
			$i=(int)$i;
			if($i>0){
				$encodedByte|=128;
			}
			$head.=chr($encodedByte);
		}
		
		fwrite($stream,$head,2);
		fwrite($stream,$buffer);
		
		$string=$this->mqttRead($stream,4);
		
		if(!((ord($string[0])>>4===2)&&($string[3]===chr(0)))){
			$this->pses->logWrite(LOG_ERR,sprintf("MQTT: %s: connection failed! (Error: 0x%02x 0x%02x)",$this->serverAddress,ord($string[0]),ord($string[3])));
			return false;
		}
		
		$this->mqttSubscribe($stream,$this->topics);
		
		$this->pses->logWrite(LOG_NOTICE,"MQTT: {}: connected",$this->serverAddress);
		$this->receiveState=new MqttReceiveState();
		
		$this->stream=$stream;
		($this->onConnect)();
		
		return true;
	}
	
	public function onDataAvailable($stream,$context){
		$receiveState=$this->receiveState;
		$commandJustReceived=false;
		if($receiveState->command===null){
			$byte=fread($stream,1);
			$receiveState->command=(int)(ord($byte)/16);
			$commandJustReceived=true;
		}
		if(($receiveState->command!==null)&&($receiveState->lengthCalculated===false)&&!($commandJustReceived&&(stream_get_meta_data($stream)["unread_bytes"]===0))){
			do{
				$digit=ord(fread($stream,1));
				$receiveState->length+=($digit&127)*$receiveState->multiplier;
				$receiveState->multiplier*=128;
			}while((stream_get_meta_data($stream)["unread_bytes"]>0)&&(($digit&128)!==0));
			
			if(($digit&128)===0){
				$receiveState->lengthCalculated=true;
			}
		}
		if($receiveState->lengthCalculated===true){
			if($receiveState->length>=1){
				$receiveState->string.=fread($stream,1);
			}
			
			while((stream_get_meta_data($stream)["unread_bytes"]>0)&&(strlen($receiveState->string)<$receiveState->length)){
				$receiveState->string.=fread($stream,min(stream_get_meta_data($stream)["unread_bytes"],$receiveState->length-strlen($receiveState->string)));
			}
			
			if(strlen($receiveState->string)===$receiveState->length){
				switch(self::KNOWN_COMMANDS[$receiveState->command]){
					case "PUBLISH":
						$receivedMessageTopicLength=(ord($receiveState->string[0])<<8)+ord($receiveState->string[1]);
						$receivedMessageTopic=substr($receiveState->string,2,$receivedMessageTopicLength);
						$receiveState->string=substr($receiveState->string,($receivedMessageTopicLength+2));
						$this->pses->logWrite(LOG_DEBUG,"MQTT: {}: message received: {}: {}",$this->serverAddress,$receivedMessageTopic,$receiveState->string);
						
						$onMessage=$this->onMessage;
						$onMessage($receivedMessageTopic,$receiveState->string);
						break;
					case "PINGRESP":
						$mqttLastPongTimestamp=time();
						$this->pses->logWrite(LOG_DEBUG,"MQTT: {}: received ping response",$this->serverAddress);
						break;
					default:
						$this->pses->logWrite(LOG_DEBUG,"MQTT: {}: received {}",$this->serverAddress,self::KNOWN_COMMANDS[$receiveState->command]);
				}
				
				$this->receiveState=new MqttReceiveState();
			}
		}
	}
	
	public function onDisconnect($stream,$context){
		$this->pses->logWrite(LOG_NOTICE,"MQTT: {}: disconnected",$this->serverAddress);
		if($this->running){
			$this->pses->logWrite(LOG_NOTICE,"MQTT: {}: reconnecting in {} seconds",$this->serverAddress,fn()=>Pses::microToSec($this->reconnectInterval));
			$this->pses->scheduleJob(Pses::microtimeInt()+$this->reconnectInterval,function(){
				$this->connect();
			});
		}
		$this->stream=null;
		($this->onDisconnect)();
	}
	
	public function onPing($stream,$context){
		if($context->lastReceivedTimestamp<=(Pses::microtimeInt()-($this->pingInterval*2))){
			$this->pses->logWrite(LOG_ERR,"MQTT: {}: ping response timeout",$this->serverAddress);
			$this->pses->disconnect($context->streamID);
		}else{
			$this->pses->logWrite(LOG_DEBUG,"MQTT: {}: sending ping",$this->serverAddress);
			$head=chr(0xc0);
			$head.=chr(0x00);
			fwrite($stream,$head,2);
		}
	}
	
	public function shutdown(){
		$this->running=false;
		if($this->isConnected()){
			$this->pses->disconnect($this->streamID);
		}
	}
	
	private function mqttSubscribe($stream,$topics,$qos=0){
		$this->pses->logWrite(LOG_INFO,"MQTT: {}: subscribing",$this->serverAddress);
		
		$i=0;
		$buffer="";
		$id=$this->messageID;
		$buffer.=chr($id>>8);
		$i++;
		$buffer.=chr($id%256);
		$i++;
		
		foreach($topics as $topic){
			$buffer.=PsesMqtt::strWriteString($topic,$i);
			$buffer.=chr(0); //QoS
			$i++;
		}
		
		$cmd=0x82;
		$cmd+=($qos<<1);
		
		$head=chr($cmd);
		$head.=PsesMqtt::setMessageLength($i);
		fwrite($stream,$head,strlen($head));
		
		$this->mqttFwrite($stream,$buffer);
		$string=$this->mqttRead($stream,2);
		
		$bytes=ord(substr($string,1,1));
		$this->mqttRead($stream,$bytes);
	}
	
	private function mqttRead($stream,$bytes=8192){
		$string="";
		$togo=$bytes;
		
		while((!feof($stream))&&($togo>0)){
			$fread=fread($stream,$togo);
			$string.=$fread;
			$togo=$bytes-strlen($string);
		}
		
		return $string;
	}
	
	private function mqttFwrite($stream,$buffer){
		$buffer_length=strlen($buffer);
		for($written=0;$written<$buffer_length;$written+=$fwrite){
			$fwrite=fwrite($stream,substr($buffer,$written));
			if($fwrite===false){
				return false;
			}
		}
		return $buffer_length;
	}
	
	private static function strWriteString($str,&$i){
		$len=strlen($str);
		$msb=$len>>8;
		$lsb=$len%256;
		$ret=chr($msb);
		$ret.=chr($lsb);
		$ret.=$str;
		$i+=($len+2);
		return $ret;
	}
	
	private static function setMessageLength($len){
		$string="";
		do {
			$digit=$len%128;
			$len>>=7;
			// If there are more digits to encode, set the top bit of this digit
			if($len>0){
				$digit|=0x80;
			}
			$string.=chr($digit);
		}while($len>0);
		return $string;
	}
}
?>
